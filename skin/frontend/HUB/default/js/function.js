jQuery(document).ready(function() {
    jQuery("select").selectBoxIt();
    jQuery('input').iCheck({
        checkboxClass: 'icheckbox_minimal',
            radioClass: 'iradio_minimal'

    });

    jQuery("#show-search").click(function() {
        var self = jQuery("#header-search");
        if(self.hasClass("fadeOut")) {
            self.removeClass("fadeOut");
        }
        self.addClass("fadeIn");

    });
    jQuery("#hide-search").click(function() {
        var self = jQuery("#header-search");
        self.addClass("skip-active");
        if(self.hasClass("fadeIn")) {
            self.removeClass("fadeIn");
        }
        self.addClass("fadeOut");
        self.removeClass("skip-active");
    });

});

//
//function showMenu() {
//    jQuery(".skip-account").click(function() {
//        jQuery('#header-account').fadeIn('slow');
//    });
//}
