<?php
class Artur_Plugin_Block_Html_BreadCrumbs extends Mage_Page_Block_Html_Breadcrumbs
{
    protected function _construct()
    {
        $this->addCrumb("firstCrumb",array(
                                        'label'=> 'Google',
                                        'title'=> 'Ultimate Search Engine',
                                        'link'=> 'http://google.com',
                                        ));
    }
}