<?php
class Artur_Plugin_Block_Registry extends Mage_Core_Block_Template
{
    public function getRegistryValue()
    {
        $var = Mage::registry('systemkey');
        return $var ? $var : "is not available";
    }
}