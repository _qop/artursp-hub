<?php
class Artur_Plugin_RenderController extends Mage_Core_Controller_Front_Action
{
    public function blockAction()
    {
        $this->getResponse()->setBody("<u>Hello Mage</u>");
    }
    public function overrideAction()
    {
        $block = $this->getLayout()->createBlock("artur_plugin/sample")->toHtml();

        $this->getResponse()->setBody($block);
    }
    public function templateAction()
    {
        $block = $this->getLayout()->createBlock("core/template")->setTemplate("artur_plugin/random.phtml")->toHtml();
        $this->getResponse()->setBody($block);
    }
    public function registryAction()
    {
        Mage::register('systemkey', '1245-A310-B13C-FF02');

        $block = $this->getLayout()->createBlock('artur_plugin/registry')->setTemplate('artur_plugin/registry.phtml')->toHtml();
        $this->getResponse()->setBody($block);
    }
    public function listBlockAction()
    {
        $tlb = $this->getLayout()->createBlock('core/text_list');
        $blockA = $this->getLayout()->createBlock('core/text')->setText('<h1>Block A</h1>');
        $blockB = $this->getLayout()->createBlock('core/text')->setText('<h1>Block B</h1>');

        $tlb->insert($blockA)->insert($blockB);
//        $this->getResponse()->setBody($tlb->toHtml());
        $this->loadLayout();
        $this->getLayout()->getBlock('content')->insert($tlb);
        $this->renderLayout();
    }

}