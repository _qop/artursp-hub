<?php

class Artur_Plugin_IndexController extends Mage_Core_Controller_Front_Action
{
    public function indexAction()
    {
        $this->loadLayout()->renderLayout();
        $key = Mage::getConfig()->getNode('default/auth/key');
        echo "<center><h3>Your key to matrix:". $key . " </h3></center>";
    }
}